<?php

require_once 'DateClass.php';

use jovix\DateClass;

$date = new DateClass('1903-01-30', '1902-04-12');

echo 'Years : '.$date->years."<br/>";
echo 'Months: '.$date->months."<br/>";
echo 'Days  : '.$date->days."<br/>";
echo 'Total Days  : '.$date->total_days."<br/>";
var_dump($date->invert);