<?php

namespace jovix;

class DateClass
{
    private static $count_seconds_in_year = 31556926; // 365 days, 5 hrs, 48 min, 46 sec.

    private static $cont_seconds_in_month = 31556926/12;


    public $years;

    public $months;

    public $days;

    public $total_days;

    public $invert = false;


    public function __construct($date_start, $date_end)
    {
        if (!self::validateDate($date_start) && self::validateDate($date_end))
            throw new \Exception('Format is not correct');


        $diff_seconds = self::convertToSecond($date_start) - self::convertToSecond($date_end) ;

        $this->setData($diff_seconds);
    }




    private function setData($diff_seconds)
    {

        if ($diff_seconds > 0){
            $this->invert = true;
        }

        $diff_seconds = abs($diff_seconds);

        $years           = $diff_seconds / self::$count_seconds_in_year;
        $years_remainder = fmod($diff_seconds, self::$count_seconds_in_year);


        if ($years < 1)
            $years_remainder = $diff_seconds;

        $month           = $years_remainder / self::$cont_seconds_in_month;
        $month_remainder = fmod($years_remainder, self::$cont_seconds_in_month);

        if ($month < 1 && $years < 1)
            $month_remainder = $diff_seconds;

        $days            = $month_remainder / 86400;
        $total_days      = $diff_seconds    / 86400;

        $this->years      =  (int)$years;
        $this->months     =  (int)$month;
        $this->days       =  (int)$days;
        $this->total_days =  (int)$total_days;

    }

    /*
    *
    */
    public static function explodeDate(string $date) : array
    {
        return explode('-', $date);
    }

    /*
     *
     */
    public static function convertToSecond(string $date){

        $array_with_date = self::explodeDate($date); // indexes = 0 - year, 1-month, 3-days.

        $seconds = ($array_with_date[0] * self::$count_seconds_in_year)  // year to seconds
                 + ($array_with_date[1] * self::$cont_seconds_in_month)  // month to seconds
                 + ( ( ($array_with_date[2] * 24) * 60 ) * 60 );  // days to seconds


        return $seconds;

    }

    /*
     *
     */
    public static function validateDate(string $date)
    {
        // from 1900
       // return preg_match('/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/',$date);

        return preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/', $date);
    }

}